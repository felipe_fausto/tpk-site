{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Contact where

import Import
-- import Network.HTTP.Types.Status
import Database.Persist.Postgresql
import qualified Data.Text as T

getContactR :: Handler Html
getContactR = do
      defaultLayout $(widgetFile "contact")
