{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.Cases where

import Import
-- import Network.HTTP.Types.Status
import Database.Persist.Postgresql
import qualified Data.Text as T

getCasesR :: Handler Html
getCasesR = do
      defaultLayout $(widgetFile "cases")
